ARG VERSION
FROM gitlab/gitlab-runner:alpine-${VERSION}
COPY config-template.toml /
ENV TEMPLATE_CONFIG_FILE /config-template.toml
